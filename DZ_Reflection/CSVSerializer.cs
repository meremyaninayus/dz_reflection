﻿using System;
using System.Text;

namespace DZ_Reflection
{
    public class CSVSerializer<T>
        where T : new()
    {
        public string Serialize(T obj)
        {
            var str = new StringBuilder();
            var type = obj.GetType();
            var fields = type.GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (var field in fields)
            {
                str.Append($"{field.Name}:{field.GetValue(obj)};");
            }
            return str.ToString();
        }

        public T Deserialize(string str)
        {
            var newObj = new T();
            var strArray = str.Split(';');
            foreach (var fieldData in strArray)
            {
                if (fieldData == "")
                    continue;
                var fieldParts = fieldData.Split(':');
                var fieldName = fieldParts[0];
                var fieldValue = fieldParts[1];
                var field = newObj.GetType().GetField(fieldName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                var fieldType = field.FieldType;
                var fieldObj = Convert.ChangeType(fieldValue, fieldType);
                field.SetValue(newObj, fieldObj);
            }
            return newObj;
        }

    }
}
