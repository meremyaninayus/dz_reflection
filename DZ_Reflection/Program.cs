﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace DZ_Reflection
{
    class Program
    {
        static void Main(string[] args)
        { 
            var serializer = new CSVSerializer<F>();

            var test = new F();

            var jsonStr = JsonConvert.SerializeObject(test.Get());
            var str = serializer.Serialize(test.Get());

            Console.WriteLine("Кастомный сериализатор "+
                MeasureTime(new Action(() => serializer.Serialize(test.Get())), 10000));
            Console.WriteLine("Newtonsoft сериализатор " +
                MeasureTime(new Action(() => JsonConvert.SerializeObject(test.Get())), 10000));
            Console.WriteLine("Кастомный десериализатор " +
                MeasureTime(new Action(()=>JsonConvert.DeserializeObject(jsonStr)), 10000));
            Console.WriteLine("Newtonsoft десериализатор " +
                MeasureTime(new Action(() => serializer.Deserialize(str)), 10000));
            Console.ReadKey();
        }

        static string MeasureTime(Action action, int tries)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < tries; i++)
            {
                action();
            }
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds.ToString();
        }
    }

    public class F 
    {   
       [JsonProperty]
        int i1, i2, i3, i4, i5;
        public int i6;
        public F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, i6=6 }; 
    }
}
